export interface StoneInterface {
    id: number;
    color: string;
    field: string;
    gipf: boolean;
}
