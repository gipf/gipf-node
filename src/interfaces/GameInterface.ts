import { Color } from '../models/enum';
import { StoneInterface } from './StoneInterface';
import { HistoryEntry, Move } from './Move';
import { GameType } from '../models/GameType';

export interface GameInterface {
  id: string;
  type: GameType;
  player_white: string;
  player_black: string;
  player_on_turn: Color;
  lastPushColor: Color;
  winner: Color;
  turnNumber: number;
  datetime_start: Date;
  datetime_last: Date;
  stones: StoneInterface[];
  possible_moves: Move[];
  history: HistoryEntry[];
  handicap_white?: number;
  handicap_black?: number;
}

export interface GameCreateInterface {
  type?: GameType;
  player_white: string;
  player_black: string;
  handicap_white?: number;
  handicap_black?: number;
}

export interface GameOverviewInterface {
  id: string;
  player_white: string;
  player_black: string;
  turnNumber: number;
  winner: Color;
  datetime_start: Date;
  datetime_last: Date;
  player_on_turn: Color;
}
