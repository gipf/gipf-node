import { Color, Direction } from '../models/enum';

export type Move = PushMove | RecoverMove;
export type MoveHistory = RecoverMove | PushMoveHistory;

export interface PushMove {
    color: Color;
    type: 'PushMove';
    field_id: string;
    direction: Direction;
};

export interface PushMoveHistory extends PushMove{
    last_pushed_field_id: string;
}

export interface RecoverMove {
    color: Color;
    type: 'RecoverMove';
    recovered_field_ids: string[];
    captured_field_ids: string[];
}

export interface HistoryEntry {
    move: MoveHistory;
    color: Color;
    turnNumber: number;
    datetime: Date;
}

export interface MovePossibilies {
    color: Color;
    moves: Move[];
};
