import { Color, Direction, opposite_direction } from './enum';
import { Field } from './Field';
import { Stone } from './Stone';
import { RecoverMove } from '../interfaces/Move';

export class RecoverMoveImpl implements RecoverMove {
  public readonly type = 'RecoverMove';

  constructor(public readonly color: Color,
                public readonly recovered_field_ids: string[],
                public readonly captured_field_ids: string[]) {
  }

  static fromField(field: Field, color: Color, direction: Direction): RecoverMoveImpl {
    let stones_in_row: Stone[] = field.getStonesInRow(direction);
    const invDir: Direction = opposite_direction.get(direction);
    const nb: Field = field.neighbor.get(invDir);
    if (nb) {
      stones_in_row = stones_in_row.concat(nb.getStonesInRow(invDir));
    }
    stones_in_row.sort();

    return new RecoverMoveImpl(
            color,
            stones_in_row.filter((s) => s.color === color).map((s) => s.field.id),
            stones_in_row.filter((s) => s.color !== color).map((s) => s.field.id));
  }

  public equals(option: RecoverMove): boolean {
    return (this.color === option.color) &&
            (this.recovered_field_ids.length === option.recovered_field_ids.length) &&
            (this.captured_field_ids.length === option.captured_field_ids.length) &&
            this.recovered_field_ids.every((f) => option.recovered_field_ids.indexOf(f) >= 0) &&
            this.captured_field_ids.every((f) => option.captured_field_ids.indexOf(f) >= 0);
  }

}
