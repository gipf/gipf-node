import { Board } from './Board';
import { Color, opposite_color } from './enum';
import { v4 as uuid } from 'uuid';
import { GameCreateInterface, GameInterface } from '../interfaces/GameInterface';
import { EventEmitter } from 'events';
import * as log from 'loglevel';
import { HistoryEntry, Move, MoveHistory, MovePossibilies } from '../interfaces/Move';
import { GameType } from './GameType';
import { init } from 'node-persist';

export class Game {

  get datetime_start(): Date {
    return this._datetime_start;
  }
  get history(): HistoryEntry[] {
    return this._history;
  }
  get turnNumber(): number {
    return this._turnNumber;
  }
  get winner(): Color {
    return this._winner;
  }
  get datetime_last(): Date {
    return this._datetime_last;
  }
  get player_on_turn(): Color {
    return this._movePossibilities.color;
  }
  get possible_moves(): Move[] {
    return this._movePossibilities.moves;
  }

  public readonly id: string;
  public readonly board: Board;
  public readonly player_white: string;
  public readonly player_black: string;
  public readonly eventEmitter: EventEmitter;
  public readonly type: GameType;

  private _movePossibilities: MovePossibilies;
  private _lastPushColor: Color;
  private _turnNumber: number;
  private _winner: Color;
  private readonly _datetime_start: Date;
  private _datetime_last: Date;
  private _history: HistoryEntry[];
  private readonly logger: log.Logger;

  constructor(initial: GameCreateInterface);
  constructor(initial: GameInterface) {
    this.eventEmitter = new EventEmitter();
    this.logger = log.getLogger(`Game-${this.id}`);

    this.id = initial.id || uuid();
    this.type = initial.type || GameType.Standard;
    this.player_white = initial.player_white;
    this.player_black = initial.player_black;
    this._winner = initial.winner || null;
    this._turnNumber = initial.turnNumber || 1;
    this._datetime_start = initial.datetime_start || new Date();
    this._datetime_last = initial.datetime_last;
    this._history = initial.history ? initial.history.slice() : [];
    this.board = new Board(initial.stones, initial.handicap_white, initial.handicap_black);

    this._lastPushColor = initial.lastPushColor || Color.black;
    this._movePossibilities = this.getMovePossibilities();
    this.type = initial.type || GameType.Simple;
  }

  public toJSON(): GameInterface {
    return {
      id: this.id,
      player_white: this.player_white,
      player_black: this.player_black,
      turnNumber: this.turnNumber,
      player_on_turn: this.player_on_turn,
      winner: this._winner,
      datetime_start: this._datetime_start,
      datetime_last: this._datetime_last,
      stones: this.board.toJSON(),
      history: this._history,
      possible_moves: this.possible_moves,
      lastPushColor: this._lastPushColor,
      type: this.type
    }
  }

  public move(move: Move): void {
    if (!this.is_move_possible(move)) {
      throw new Error(`Move not possible.`);
    }
    let hmove: MoveHistory;
    if (move.type === 'RecoverMove') {
      this.board.performRecoverMove(move);
      hmove = move;
    } else if (move.type === 'PushMove') {
      hmove = this.board.performPushMove(move);
      this._lastPushColor = move.color;
    }
    this.performActionAfterMove(hmove);
    this.eventEmitter.emit('moveDone', this.toJSON());
    if (this._movePossibilities.moves.length === 0) {
      this._winner = opposite_color(this._movePossibilities.color);
      this.logger.info('Game Over');
      this.logger.info('Winner: ', this.winner);
      this.eventEmitter.emit('gameover');
    }
  }

  public undo(): void {
    const h: HistoryEntry = this.history.pop();
    this.board.undoMove(h.move);
    this._turnNumber = this._turnNumber - 1;
    this._datetime_last = this.history.length ? this.history[this.history.length - 1].datetime : undefined;
    this._lastPushColor = h.move.type === 'PushMove' ? opposite_color(h.color) : h.color;
    this._movePossibilities = this.getMovePossibilities();
  }

  public is_move_possible(move: Move): boolean {
    if (!move) {
      return false;
    }
    if (move.color !== this._movePossibilities.color) {
      this.logger.trace('Wrong player wants to move');
      return false;
    }
    const index: number = this._movePossibilities.moves.findIndex((m: Move) => {
      if (m.color !== move.color) { return false }
      if (m.type === 'PushMove' && move.type === 'PushMove') {
        return m.direction === move.direction &&
                    m.field_id === move.field_id;
      }
      if (m.type === 'RecoverMove' && move.type === 'RecoverMove') {
        return m.captured_field_ids.every((elem, index) => elem === move.captured_field_ids[index]) &&
                    m.recovered_field_ids.every((elem, index) => elem === move.recovered_field_ids[index]);
      }
      return false
    });
    return index !== -1;
  }

  public getMovePossibilities(): MovePossibilies {
    const moves: Move[] = this.board.getPossibleRecoverMoves(this._lastPushColor);
    if (moves.length > 0) {
      return { moves: moves, color: this._lastPushColor };
    }

    const lastPushOppositeColor: Color = opposite_color(this._lastPushColor);
    const moves_opponent: Move[] = this.board.getPossibleRecoverMoves(lastPushOppositeColor);
    if (moves_opponent.length > 0) {
      return { moves: moves_opponent, color: lastPushOppositeColor };
    }

    if (this.board.reserve(lastPushOppositeColor).stones.length > 0) {
      const push_moves: Move[] = this.board.getPossiblePushMoves(lastPushOppositeColor);
      return { moves: push_moves, color: lastPushOppositeColor };
    }

    // Game over - no move possible
    return { moves: [], color: lastPushOppositeColor };
  }

  public set_root_position(position: { white: string[]; black: string[] }, startingColor: Color): void {
    this.board.set_root_position(position);
    this._lastPushColor = opposite_color(startingColor);
    this._movePossibilities = this.getMovePossibilities();
  }

  private performActionAfterMove(move: MoveHistory): void {
    this._datetime_last = new Date();
    this._history.push({
      color: move.color,
      move: move,
      turnNumber: this.turnNumber,
      datetime: this.datetime_last
    });
    this._turnNumber = this._turnNumber + 1;
    this._movePossibilities = this.getMovePossibilities();
  }
}
