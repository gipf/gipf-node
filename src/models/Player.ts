import { sign } from 'jsonwebtoken';
import * as config from 'config';
export class Player {
  public readonly name: string;

  constructor(name: string) {
    this.name = name;
  }
}

export class User extends Player {

  public readonly email: string;
  public readonly isAdmin: boolean;
  public readonly password: string;

  constructor(initial: {name: string, password: string, email: string}) {
    super(initial.name);
    this.password = initial.password;
    this.email = initial.email;
    this.isAdmin = false;
  }

  public getInitial(): string {
    return this.name.charAt(0);
  }

  public toJSON(): Object {
    return { name: this.name, password: this.password, email: this.email };
  }

  public generateAuthToken(): string {
    // get the private key from the config file -> environment variable
    const token: string = sign({ _id: this.name, isAdmin: this.isAdmin }, config.get('myprivatekey'));
    return token;
  }

}
