export enum Color {
    white = 'white',
    black = 'black'
}

export function colorFromString(color: string): Color {
  return Color[color];
}

export function opposite_color(color: Color): Color {
  if (color === Color.black) {
    return Color.white;
  } else if (color === Color.white) {
    return Color.black;
  } else {
    return null;
  }
}

export enum Direction {
    n = 'n',
    ne = 'ne',
    se = 'se',
    s = 's',
    sw = 'sw',
    nw = 'nw'
}

export const clockwise: Map<Direction, Direction> = new Map([
    [ Direction.n, Direction.ne ],
    [ Direction.ne, Direction.se ],
    [ Direction.se, Direction.s ],
    [ Direction.s, Direction.sw ],
    [ Direction.sw, Direction.nw ],
    [ Direction.nw, Direction.n ]
]);

export const counterclockwise: Map<Direction, Direction> = new Map([
    [ Direction.n, Direction.nw ],
    [ Direction.ne, Direction.n ],
    [ Direction.se, Direction.ne ],
    [ Direction.s, Direction.se ],
    [ Direction.sw, Direction.s ],
    [ Direction.nw, Direction.sw ]
]);

export const opposite_direction: Map<Direction, Direction> = new Map([
    [ Direction.n, Direction.s ],
    [ Direction.ne, Direction.sw ],
    [ Direction.se, Direction.nw ],
    [ Direction.s, Direction.n ],
    [ Direction.sw, Direction.ne ],
    [ Direction.nw, Direction.se ]
]);
