import { Stone } from './Stone';

export abstract class BasicField {
  public readonly id: string;
  constructor(id: string) {
    this.id = id;
  }

  public abstract addStone(stone: Stone): void;
  public abstract removeStone(stone: Stone): void;

}
