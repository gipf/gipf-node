import { BasicField } from './BasicField';
import { Stone } from './Stone';

/**
 * A Field that can hold multiple stones as they are reserve and out
 */
export class MultiField extends BasicField {

  public readonly stones: Stone[] = [];

  public addStone(stone: Stone): void {
    this.stones.push(stone);
  }

  public removeStone(stone: Stone): void {
    const index: number = this.stones.indexOf(stone);
    if (index > -1) {
      this.stones.splice(index, 1);
    }
  }
}
