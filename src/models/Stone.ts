import { Color } from './enum';
import { BasicField } from './BasicField';
import { StoneInterface } from '../interfaces/StoneInterface';

export class Stone {

  public readonly id: number;
  public readonly color: Color;
  public gipf: boolean;
  public field: BasicField;

  constructor(id: number, color: string, field: BasicField = null) {
    this.id = id;
    this.color = color as Color;
    this.gipf = false;
    if (field) {
      this.set(field);
    }
  }

  public toJson(): StoneInterface {
    return {
      id: this.id,
      color: this.color.toString(),
      gipf: this.gipf,
      field: this.field ? this.field.id : null
    };
  }

  public set(newField: BasicField): void {
    const oldField: BasicField = this.field;
    if (oldField) {
      oldField.removeStone(this);
    }

    newField.addStone(this);
    this.field = newField;
  }
}
