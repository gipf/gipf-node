import { Stone } from './Stone';
import { Color, Direction } from './enum';
import { BasicField } from './BasicField';

export class Field extends BasicField {

  public border: boolean = false;

  public stone: Stone;

  public readonly neighbor: Map<Direction, Field> = new Map<Direction, Field>();

  public is_free(): boolean {
    return ((this.stone === null || this.stone === undefined) && !this.border);
  }

  public addStone(stone: Stone): void {
    this.stone = stone;
  }

  public removeStone(stone: Stone): void {
    this.stone = null;
  }

  /** returns last field which has been pushed to
   *
   * @param direction
   */
  public move_neighbor(direction: Direction): string {
    let last_field_id: string = this.id;
    if (!this.neighbor.get(direction).is_free()) {
      last_field_id = this.neighbor.get(direction).move_neighbor(direction);
    }
    this.stone.set(this.neighbor.get(direction));
    return last_field_id;
  }

  public pull_neighbor(direction: Direction, last_field_id: string): void {
    const field: Field = this.neighbor.get(direction);
    if (field.stone) {
      field.stone.set(this);
      if (last_field_id !== this.id) {
        field.pull_neighbor(direction, last_field_id);
      }
    }
  }

  public is_line_free(direction: Direction): boolean {
    return (this.is_free() ||
            (this.neighbor.has(direction) && this.neighbor.get(direction).is_line_free(direction))
    );
  }

  public nrOfStonesInRowWithColor(color: Color, direction: Direction): number {
    if (this.stone && this.stone.color === color) {
      const neighbor: Field = this.neighbor.get(direction);
      if (neighbor) {
        return 1 + neighbor.nrOfStonesInRowWithColor(color, direction);
      } else {
        return 1;
      }
    } else {
      return 0;
    }
  }

    // returns stone
  public getStonesInRow(direction: Direction): Stone[] {
    if (this.stone) {
      const neighbor: Field = this.neighbor.get(direction);
      if (neighbor) {
        return [ this.stone, ...this.neighbor.get(direction).getStonesInRow(direction) ];
      } else {
        return [ this.stone ];
      }
    } else {
      return [];
    }
  }
}
