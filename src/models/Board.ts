import { Stone } from './Stone';
import { Field } from './Field';
import { MultiField } from './MultiField';
import { BasicField } from './BasicField';
import { Color, Direction, opposite_color, opposite_direction } from './enum';
import { StoneInterface } from '../interfaces/StoneInterface';
import { RecoverMoveImpl } from './RecoverMove';
import { MoveHistory, PushMove, PushMoveHistory, RecoverMove } from '../interfaces/Move';
import * as log from 'loglevel';
import { v4 as uuid } from 'uuid';

export class Board {
  public readonly stones: Stone[];
  public readonly fields: Map<string, Field> = new Map<string, Field>();
  public readonly reserve_white: MultiField = new MultiField('reserve_white');
  public readonly reserve_black: MultiField = new MultiField('reserve_black');
  public readonly out: MultiField = new MultiField('out');
  private border_fields: Field[];
  private readonly logger: log.Logger = log.getLogger('Board');

  constructor(initial: StoneInterface[] = undefined, handicap_white: number = 0, handicap_black: number = 0) {
    this.create_fields();
    if (initial === undefined) {
      this.stones = this.create_stones(15 - handicap_white, 15 - handicap_black);
    } else {
      this.stones = initial.map(
        (inital_stone: StoneInterface) => {
          const field: BasicField = this.fields.get(inital_stone.field) || this.reserve(inital_stone.color as Color);
          return new Stone(inital_stone.id, inital_stone.color, field);
        }
      );
    }
  }

  public set_root_position(rootPosition: { white: string[], black: string[] }): void {
    // set all stones back to root position
    this.stones.forEach((stone) => stone.set(this.reserve(stone.color)));

    rootPosition.white.forEach((field_id) => this.reserve_white.stones[0].set(this.fields.get(field_id)));
    rootPosition.black.forEach((field_id) => this.reserve_black.stones[0].set(this.fields.get(field_id)));
  }

  public toJSON(): StoneInterface[] {
    return this.stones.map((stone: Stone) => stone.toJson());
  }

  public getPossiblePushMoves(color?: Color): PushMove[] {
    return this.border_fields.reduce((moves: PushMove[], field: Field) => {
      field.neighbor.forEach((value: Field, key: Direction) => {
        if (field.is_line_free(key)) {
          moves.push({ type: 'PushMove', field_id: field.id, direction: key, color: color })
        }
      });
      return moves;
    }, []);
  }

  public getPossibleRecoverMoves(color: Color): RecoverMove[] {
    const move_options: RecoverMove[] = [];
    this.fields.forEach((field: Field) => {
      for (const direction of [ Direction.n, Direction.ne, Direction.nw ]) {
        const number: number = field.nrOfStonesInRowWithColor(color, direction);
        if (number >= 4) {
          const newRecoverMove: RecoverMoveImpl = RecoverMoveImpl.fromField(field, color, direction);
          if (move_options.findIndex((move) => newRecoverMove.equals(move)) === -1) {
            move_options.push(newRecoverMove);
          }
        }
      }
    });
    return move_options;
  }

  public performPushMove(move: PushMove): PushMoveHistory {
    const stone: Stone = this.get_stone_from_reserve(move.color);
    const field: Field = this.fields.get(move.field_id);
    if (!field.is_line_free(move.direction)) {
      throw new Error('No field free in line');
    }
    // set stone
    stone.set(field);

    // move stones
    const last_field_push_id: string = field.move_neighbor(move.direction);

    return { ...move, last_pushed_field_id: last_field_push_id };
  }

  public performRecoverMove(recoverMove: RecoverMove): void {
    this.logger.debug('perform recover move', recoverMove);
    recoverMove.recovered_field_ids.forEach((field) => {
      const stone: Stone = this.fields.get(field).stone;
      if (stone.gipf) {
        stone.gipf = false;
        const newStone: Stone = new Stone(uuid(), stone.color, this.reserve(stone.color));
      }
      stone.set(this.reserve(recoverMove.color));
    });
    recoverMove.captured_field_ids.forEach((field) => {
      this.fields.get(field).stone.set(this.out);
    });
  }

  public undoMove(move: MoveHistory): void {
    if (move.type === 'RecoverMove') {
      move.recovered_field_ids.forEach((field_id) => {
        this.reserve(move.color).stones[0].set(this.fields.get(field_id));
      });
      move.captured_field_ids.forEach((field_id) => {
        this.out.stones
          .find((st) => st.color === opposite_color(move.color))
          .set(this.fields.get(field_id));
      });
    } else if (move.type === 'PushMove') {
      this.logger.trace('undo', move);
      const field: Field = this.fields.get(move.field_id);
      field.pull_neighbor(move.direction, move.last_pushed_field_id);
      this.logger.trace(`${field.id} -> reserve: ${field.stone ? field.stone.id : undefined}`);
      field.stone.set(this.reserve(field.stone.color));
    }
  }

  public reserve(color: Color): MultiField {
    return (color === Color.white) ? this.reserve_white : this.reserve_black;
  };

  public mergeToGipfStone(color: Color): Stone {
    if (this.reserve(color).stones.length >= 2) {
      this.reserve(color).stones.pop();
      const stone: Stone = this.reserve(color).stones.pop();
      stone.gipf = true;
      return stone;
    } else {
      throw new Error('Could not merge gipf stone');
    }
  }

  private get_stone_from_reserve(color: Color): Stone {
    return this.reserve(color).stones.find(() => true);
  }

  private create_fields_in_row(row: string, number: number): Field[] {
    const temp_row: Field[] = [];
    for (let i: number = 1; i <= number; i++) {
      temp_row.push(new Field(row + i.toString()));
    }
    return temp_row;
  }

  private create_fields(): void {
    const row_a: Field[] = this.create_fields_in_row('a', 5);
    const row_b: Field[] = this.create_fields_in_row('b', 6);
    const row_c: Field[] = this.create_fields_in_row('c', 7);
    const row_d: Field[] = this.create_fields_in_row('d', 8);
    const row_e: Field[] = this.create_fields_in_row('e', 9);
    const row_f: Field[] = this.create_fields_in_row('f', 8);
    const row_g: Field[] = this.create_fields_in_row('g', 7);
    const row_h: Field[] = this.create_fields_in_row('h', 6);
    const row_i: Field[] = this.create_fields_in_row('i', 5);

    const rows: Field[][] = [ row_a, row_b, row_c, row_d, row_e, row_f, row_g, row_h, row_i ];
    rows.forEach((row: Field[]) => row.forEach(
      (field: Field) => this.fields.set(field.id, field)
    ));

    // set border fields
    this.border_fields = [].concat(row_a, row_i);
    for (const row of [ row_b, row_c, row_d, row_e, row_f, row_g, row_h ]) {
      this.border_fields = this.border_fields.concat(row[0], row[row.length - 1]);
    }
    for (const field of this.border_fields) {
      field.border = true;
    }

    // Connect fields
    // nw -> se
    for (let i = 1; i < 5; i++) {
      this.connect_fields(row_a[i], row_b[i], Direction.se);
      this.connect_fields(row_h[i], row_i[i - 1], Direction.se);
    }
    for (let i = 1; i < 6; i++) {
      this.connect_fields(row_b[i], row_c[i], Direction.se);
      this.connect_fields(row_g[i], row_h[i - 1], Direction.se);
    }
    for (let i = 1; i < 7; i++) {
      this.connect_fields(row_c[i], row_d[i], Direction.se);
      this.connect_fields(row_f[i], row_g[i - 1], Direction.se);
    }
    for (let i = 1; i < 8; i++) {
      this.connect_fields(row_d[i], row_e[i], Direction.se);
      this.connect_fields(row_e[i], row_f[i - 1], Direction.se);
    }

    // n -> s
    for (const row of rows) {
      for (let i = 1; i < row.length; i++) {
        this.connect_fields(row[i], row[i - 1], Direction.s);
      }
    }

    // ne -> sw
    for (let i = 1; i < 5; i++) {
      this.connect_fields(row_a[i - 1], row_b[i], Direction.ne);
      this.connect_fields(row_h[i], row_i[i], Direction.ne);
    }
    for (let i = 1; i < 6; i++) {
      this.connect_fields(row_b[i - 1], row_c[i], Direction.ne);
      this.connect_fields(row_g[i], row_h[i], Direction.ne);
    }
    for (let i = 1; i < 7; i++) {
      this.connect_fields(row_c[i - 1], row_d[i], Direction.ne);
      this.connect_fields(row_f[i], row_g[i], Direction.ne);
    }
    for (let i = 1; i < 8; i++) {
      this.connect_fields(row_d[i - 1], row_e[i], Direction.ne);
      this.connect_fields(row_e[i], row_f[i], Direction.ne);
    }
  }

  private connect_fields(field1: Field, field2: Field, direction: Direction): boolean {
    if (field1.border && field2.border) {
      return true;
    }
    field1.neighbor.set(direction, field2);
    field2.neighbor.set(opposite_direction.get(direction), field1);
  }

  private create_stones(number_white: number = 15, number_black: number = 15): Stone[] {
    const stones: Stone[] = [];
    for (let i: number = 1; i <= number_white; i++) {
      stones.push(new Stone(i, Color.white, this.reserve_white));
    }
    for (let i: number = 1; i <= number_black; i++) {
      stones.push(new Stone(15 + i, Color.black, this.reserve_black));
    }
    return stones;
  }
}
