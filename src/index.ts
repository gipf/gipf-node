import * as debug from 'debug';
import { Server } from './server/server';
import * as serverHandlers from './server/serverHandlers';
import { Manager } from './manager/Manager';
import * as prefix from 'loglevel-plugin-prefix';
import * as log from 'loglevel';
import * as chalk from 'chalk';

debug('ts-express:server');

const colors = {
    TRACE: chalk.magenta,
    DEBUG: chalk.cyan,
    INFO: chalk.blue,
    WARN: chalk.yellow,
    ERROR: chalk.red,
};

log.setDefaultLevel(log.levels.INFO);
prefix.reg(log);
prefix.apply(log, {
    format(level, name, timestamp) {
        return `${chalk.gray(`[${timestamp}]`)} ${colors[level.toUpperCase()](level)} ${chalk.green(`${name}:`)}`;
    },
});

const port: number | string | boolean = serverHandlers.normalizePort(process.env.PORT || 3000);

const manager: Manager = new Manager();
const gipfApp: Server = new Server(manager);
gipfApp.startHttpServer(port);
gipfApp.initSocketServer();
log.info(`Server listening on port ${port}`);
