import { Manager } from '../manager/Manager';
import { Game } from '../models/Game';
import { ConsoleWriter } from './ConsoleWriter';

const manager: Manager = new Manager();

const game: Game = manager.createGame({ player_white: 'AlphaBeta2', player_black: 'AlphaBeta2' });

game.eventEmitter.on('moveDone', () => {
  ConsoleWriter.write(game);
  ConsoleWriter.writeStatus(game);
}).on('gameover', () => {
  console.log('==> Game over <==');
  console.log(`Winner: ${game.winner}`);
});
