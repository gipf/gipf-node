import { Game } from '../models/Game';
import { Color } from '../models/enum';
import { Stone } from '../models/Stone';
import { Field } from '../models/Field';

export class ConsoleWriter {

  private static output: {}[][] = [
    [ '          i5  i4  i3  i2  i1' ],
    [ '           •   •   •   •   •' ],
    [ '            \\ / \\ / \\ / \\ /' ],
    [ '      h6 •---', { field: 'h5' }, '---', { field: 'h4' }, '---', { field: 'h3' }, '---', { field: 'h2' }, '---• h1' ],
    [ '          \\ / \\ / \\ / \\ / \\ /' ],
    [ '    g7 •---', { field: 'g6' }, '---', { field: 'g5' }, '---', { field: 'g4' }, '---', { field: 'g3' }, '---', { field: 'g2' }, '---• g1' ],
    [ '        \\ / \\ / \\ / \\ / \\ / \\ /' ],
    [ '  f8 •---', { field: 'f7' }, '---', { field: 'f6' }, '---', { field: 'f5' }, '---', { field: 'f4' }, '---', { field: 'f3' }, '---', { field: 'f2' }, '---• f1' ],
    [ '      \\ / \\ / \\ / \\ / \\ / \\ / \\ /' ],
    [ 'e9 •---', { field: 'e8' }, '---', { field: 'e7' }, '---', { field: 'e6' }, '---', { field: 'e5' }, '---', { field: 'e4' }, '---', { field: 'e3' }, '---', { field: 'e2' }, '---• e1' ],
    [ '      / \\ / \\ / \\ / \\ / \\ / \\ / \\ ' ],
    [ '  d8 •---', { field: 'd7' }, '---', { field: 'd6' }, '---', { field: 'd5' }, '---', { field: 'd4' }, '---', { field: 'd3' }, '---', { field: 'd2' }, '---• d1' ],
    [ '        / \\ / \\ / \\ / \\ / \\ / \\ ' ],
    [ '    c7 •---', { field: 'c6' }, '---', { field: 'c5' }, '---', { field: 'c4' }, '---', { field: 'c3' }, '---', { field: 'c2' }, '---• c1' ],
    [ '          / \\ / \\ / \\ / \\ / \\ ' ],
    [ '      b6 •---', { field: 'b5' }, '---', { field: 'b4' }, '---', { field: 'b3' }, '---', { field: 'b2' }, '---• b1' ],
    [ '            / \\ / \\ / \\ / \\ ' ],
    [ '           •   •   •   •   •' ],
    [ '          a5  a4  a3  a2  a1' ]
  ];

  public static write(game: Game): void {
    for (const line of this.output) {
      const token: string[] = line.map((token: (string | {field: string})) => {
        if (typeof(token) === 'string') {
          return token;
        } else {
          const field: Field = game.board.fields.get(token.field);
          if (!field.stone) {
            return ' ';
          }
          if (field.stone.color === Color.white) {
            return '○';
          } else {
            return '●';
          }
        }
      });
      console.info(token.join(''))
    }
  }

  public static writeStatus(game: Game) {
    console.log(`White: ${game.board.reserve_white.stones.length}/${15-game.board.reserve_white.stones.length- game.board.out.stones.filter(s => s.color === Color.white).length}/${game.board.out.stones.filter(s => s.color === Color.white).length}`);
    console.log(`Black: ${game.board.reserve_black.stones.length}/${15-game.board.reserve_black.stones.length- game.board.out.stones.filter(s => s.color === Color.black).length}/${game.board.out.stones.filter(s => s.color === Color.black).length}`);
  }

}

