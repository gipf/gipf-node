import * as log from 'loglevel';
import * as storage from 'node-persist';
import * as WebSocket from 'ws';
import { AI } from '../ai/AI';
import { AiFactory } from '../ai/AiFactory';
import { GameCreateInterface, GameInterface } from '../interfaces/GameInterface';
import { Color } from '../models/enum';
import { Game } from '../models/Game';
import { User } from '../models/Player';

export class Manager {
  private readonly users: User[] = [];
  private readonly games: Game[] = [];
  private readonly subscriptions: Map<string, WebSocket[]> = new Map<string, WebSocket[]>();
  private readonly logger: log.Logger;

  constructor() {
    this.logger = log.getLogger('Manager');
  }

  public createGame(initial: GameCreateInterface): Game {
    this.logger.debug('create game', initial);
    const game: Game = new Game(initial);
    if (AiFactory.getIdentifiers().indexOf(initial.player_black) !== -1) {
      const ai: AI = AiFactory.create(initial.player_black);
      game.eventEmitter.on('moveDone', async (gameI: GameInterface) => {
        if (gameI.player_on_turn === Color.black && game.possible_moves.length > 0) {
          game.move(await ai.getBestMove(game));
        }
      });
    }
    if (AiFactory.getIdentifiers().indexOf(initial.player_white) !== -1) {
      const ai: AI = AiFactory.create(initial.player_white);
      game.eventEmitter.on('moveDone', async (gameI: GameInterface) => {
        if (gameI.player_on_turn === Color.white && game.possible_moves.length > 0) {
          game.move(await ai.getBestMove(game));
        }
      });
      ai.getBestMove(game)
        .then((move) => game.move(move))
        .catch((err) => this.logger.error(err));
    }

    this.createNotification(game);
    this.games.push(game);
    return game;
  }

  public getGames(): Game[] {
    return this.games;
  }

  public getGame(gameId: string): Game {
    const game: Game = this.games.find((g) => g.id === gameId);
    if (!game) {
      throw new Error(`Game with id ${gameId} not found`);
    } else {
      return game;
    }
  }

  public saveToStorage(game: Game): void {
    storage.set(`game-${game.id}`, game.toJSON())
            .catch((err) => this.logger.warn('Could not save game', game.id, err));
  }

  public async createUser(name: string, password: string, email: string): Promise<User> {
    const user: User = new User({ name, password, email });
    this.users.push(user);
    await storage.set(`player-${user.name}`, user.toJSON());
    return user;
  }

  public getUser(name: string): User {
    return this.users.find((g) => g.name === name);
  }

  public async loadFromStorage(): Promise<void> {
    this.logger.debug('loading games');
    const gamesSerializations: GameInterface[] = await storage.valuesWithKeyMatch('game-');
    this.logger.debug('Loaded game serializations', gamesSerializations.length);
    gamesSerializations.forEach((gameSerialization) => this.createGame(gameSerialization));
    this.games.forEach((game: Game) => this.createNotification(game));
    this.logger.info(`Loaded ${this.games.length} games`);

    const pl = await storage.valuesWithKeyMatch('player');
    pl.forEach((p) => this.users.push(new User(p)));
    this.logger.info(`Loaded ${pl.length} players`);
  }

  public subscribe(id: string, ws: WebSocket): void {
    const subscriptions: WebSocket[] = this.subscriptions.get(id);
    if (subscriptions) {
      if (subscriptions.indexOf(ws) === -1) {
        subscriptions.push(ws);
      }
    } else {
      this.subscriptions.set(id, [ ws ]);
    }
    this.logger.debug(`added subscription for game ${id}`, ws.eventNames());
  }

  public unsubscribe(id: string, ws: WebSocket): void {
    const array: WebSocket[] = this.subscriptions.get(id);
    const index: number = array.indexOf(ws);
    if (index > -1) {
      array.splice(index, 1);
    }
    this.logger.info('removed subscription', ws);
  }

  private createNotification(game: Game): void {
    this.logger.debug(`create notification for game ${game.id}`);
    game.eventEmitter.on('moveDone', () => {
      const subscribers: WebSocket[] = this.subscriptions.get(game.id) || [];
      subscribers.forEach((ws: WebSocket) => {
        ws.send(JSON.stringify(game.toJSON()));
      });
      this.logger.info('send move to', subscribers.length);
    });
  }
}
