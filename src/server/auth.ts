import { verify } from 'jsonwebtoken';
import { get as configGet } from 'config';
import { NextFunction, Request, Response } from 'express';

export function auth(req: Request, res: Response, next: NextFunction) {
    // get the token from the header if present
  const token: string = req.header('x-access-token') || req.header('authorization');
    // if no token found, return response (without going to the next middelware)
  if (!token) {
    return res.status(401).send('Access denied. No token provided.');
  }

  try {
    // if can verify the token
    const decoded = verify(token, configGet('myprivatekey'));
    //console.log('decoded', decoded);
    res.locals.user = decoded;
    next();
  } catch (ex) {
    // if invalid token
    res.status(400).send('Invalid token.');
  }
}
