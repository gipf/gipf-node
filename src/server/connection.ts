import * as storage from 'node-persist';
import { Manager } from '../manager/Manager';

export default class Connection {
  static init(manager: Manager): void {
    storage.init()
      .then(() => manager.loadFromStorage())
      .catch((err) => console.log('something wrong', err));
  }
}
