import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as cors from 'cors';
import * as helmet from 'helmet';
import * as express from 'express';
import { Manager } from '../manager/Manager';

export default class Middleware {
  static init(app: express.Application, manager: Manager): void {

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(compression());
    app.use(helmet());
    app.use(cors());

    app.set('manager', manager);
  }
}
