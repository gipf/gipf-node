import * as express from 'express';
import { GameController } from './GameController';
import * as asyncHandler from 'express-async-handler';
import { auth } from './auth';
import { Manager } from '../manager/Manager';
import { User } from '../models/Player';
import { compare, hash } from 'bcrypt';

export default class Routes {
    static init(app: express.Application): void {
        const gameRouter: express.Router = express.Router();
        const gameController = new GameController();

        app.use('/doc', express.static('apidoc'));

        app.use('/api', gameRouter);

        /**
         * @api {get} /games    Get all games
         * @apiName GetGames
         * @apiGroup Manager
         */
        gameRouter.get('/games', asyncHandler(gameController.getGames));

        /**
         * @api {get} /game/:id    Get game
         * @apiName GetGame
         * @apiGroup Game
         * @apiParam {string} id    Games unique ID.
         */
        gameRouter.get('/game/:id', asyncHandler(gameController.getGame));

        /**
         * @api {post} /game    Create game
         * @apiName CreateGame
         * @apiGroup Game
         * @apiParam {string} player_white
         * @apiParam {string} player_black
         */
        gameRouter.post('/game', asyncHandler(gameController.createGame));

        /**
         * @api {post} /game/:id/move   Send a move to game
         * @apiName MovePush
         * @apiGroup Game
         * @apiParam {string} id    Games unique ID.
         * @apiParam {string="PushMove"} type="PushMove"
         * @apiParam {string} field_id id of field
         * @apiParam {string="n","ne","nw","se","sw","s"} direction
         * @apiParam {string="white","black"} color
         */
        gameRouter.post('/game/:id/move', asyncHandler(gameController.moveGame));

        /**
         * @api {post} /game/:id/move   Send a recover move to game
         * @apiName  MoveRecover
         * @apiGroup Game
         * @apiParam {string} id    Games unique ID.
         * @apiParam {string} field_id id of field
         * @apiParam {string="n","ne","nw","se","sw","s"} direction
         * @apiParam {string="white","black"} color
         */
        gameRouter.post('/game/:id/move', asyncHandler(gameController.moveGame));

        /**
         * @api {get} /current   Get current user via JWT
         * @apiName  UserGet
         * @apiGroup User
         */
        gameRouter.get('/current', auth, async (req, res) => {
            const manager: Manager = req.app.get('manager');
            const user: User = manager.getUser(res.locals.user._id);
            res.send(user.toJSON());
        });

        /**
         * @api {post} /register   Register new user
         * @apiName  UserRegister
         * @apiGroup User
         * @apiParam {string} name
         * @apiParam {string} password
         * @apiParam {string} email
         */
        gameRouter.post('/register', async (req, res) => {
            // validate the request body first
            //const { error } = validate(req.body);
            //if (error) return res.status(400).send(error.details[0].message);

            // find an existing user
            const manager: Manager = req.app.get('manager');
            let user: User = manager.getUser(req.body.name);
            if (user) {
                return res.status(400).send('User already registered.');
            }
            const hashed_password: string = await hash(req.body.password, 10);
            user = await manager.createUser(req.body.name, hashed_password, req.body.email);

            const token: string = user.generateAuthToken();
            res.header('x-auth-token', token).send({
                _id: user.name,
                name: user.name,
                email: user.email
            });
        });

        /**
         * @api {post} /login   Login user
         * @apiName  UserLogin
         * @apiGroup User
         * @apiParam {string} name
         * @apiParam {string} password
         */
        gameRouter.post('/login', async (req, res) => {
            const manager: Manager = req.app.get('manager');
            const user: User = manager.getUser(req.body.name);
            if (!user) {
                return res.status(400).send('User does not exist.');
            }
            if (await compare(req.body.password, user.password)) {
                const token: string = user.generateAuthToken();
                res.header('Authorization', token).header('x-auth-token', token).send({
                    _id: user.name,
                    name: user.name,
                    email: user.email,
                    token: token
                });
            } else {
                return res.status(400).send('Password not correct.')
            }
        });
    }
}
