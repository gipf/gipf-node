import * as express from 'express';
import { Game } from '../models/Game';
import { GameOverviewInterface } from '../interfaces/GameInterface';
import { Manager } from '../manager/Manager';

export class GameController {
  public getGame(req: express.Request, res: express.Response, next: express.NextFunction): void {
    const manager: Manager = req.app.get('manager');
    const game: Game = manager.getGame(req.params.id);
    if (game) {
      res.json(game.toJSON());
    } else {
      res.status(500).send('Game does not exist');
    }
  }

  public moveGame(req: express.Request, res: express.Response, next: express.NextFunction): void {
    const manager: Manager = req.app.get('manager');
    const game: Game = manager.getGame(req.params.id);
    if (game) {
      game.move(req.body);
      manager.saveToStorage(game);
      res.json(game.toJSON());
    } else {
      res.status(500).send('Game does not exist');
    }
  }

  public getGames(req: express.Request, res: express.Response, next: express.NextFunction): void {
    const manager: Manager = req.app.get('manager');
    const games_overview: GameOverviewInterface[] = manager.getGames().map(
            (game: Game) => {
              return {
                id: game.id,
                player_white: game.player_white,
                player_black: game.player_black,
                turnNumber: game.turnNumber,
                winner: game.winner,
                datetime_start: game.datetime_start,
                datetime_last: game.datetime_last,
                player_on_turn: game.player_on_turn
              };
            });
    res.json(games_overview);
  }

  public createGame(req: express.Request, res: express.Response, next: express.NextFunction): void {
    const manager: Manager = req.app.get('manager');
    const game: Game = manager.createGame(
            { player_white: req.body.player_white, player_black: req.body.player_black });
    manager.saveToStorage(game);
    res.json(game.toJSON());
  }
}
