import * as express from 'express';
import Routes from './routes';
import Middleware from './middleware';
import Connection from './connection';
import * as http from 'http';
import * as WebSocket from 'ws';
import * as serverHandlers from './serverHandlers';
import { Manager } from '../manager/Manager';

export interface WsMessage {
  message: 'subscribe' | 'unsubscribe';
  id: string;
}
export class Server {

  public app: express.Application;
  public wss: WebSocket.Server;
  private httpServer: http.Server;
  private interval: NodeJS.Timer;
  private readonly manager: Manager;

  constructor(manager: Manager) {
    this.app = express();
    this.manager = manager;
    Middleware.init(this.app, manager);
    Connection.init(manager);
    Routes.init(this.app);
  }

  public startHttpServer(port: number | string | boolean): void {
    this.httpServer = http.createServer(this.app);

    this.app.set('port', port);
    this.httpServer.listen(port);
    this.httpServer.on('error', (error) => serverHandlers.onError(error, port));
    this.httpServer.on('listening', () => {
      const addr: any = this.httpServer.address();
      const bind: string = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`;
    });
  }

  public async stop(): Promise<void> {
    global.clearInterval(this.interval);
    if (this.httpServer) {
      await new Promise((resolve) => this.httpServer.close(resolve));
      this.httpServer = null;
    }
    if (this.wss) {
      await new Promise(resolve => this.wss.close(resolve));
      this.wss = null;
    }
  }

  public initSocketServer(): void {
    if (this.httpServer) {
      this.wss = new WebSocket.Server({ server: this.httpServer });
      this.wss.on('connection', (ws: WebSocket, req: http.IncomingMessage) => {
        this.interval = global.setInterval(() => ws.ping(), 3000);
        ws.on('message', (data) => {
          try {
            const json: WsMessage = JSON.parse(data.toString());
            if (json.message === 'subscribe') {
              this.manager.subscribe(json.id, ws);
            } else if (json.message === 'unsubscribe') {
              this.manager.unsubscribe(json.id, ws);
            }
          } catch (e) {
            console.log(e);
          }
        });
      });
    } else {
      throw new Error('Running HTTP server is required');
    }
  }

  /** Notify all clients via web sockets about refresh of data
   */
  private notifyClients(notification: Object): void {
    if (this.wss) {
      this.wss.clients.forEach((client: WebSocket) => {
        if (client.readyState === WebSocket.OPEN) {
          client.send(JSON.stringify(notification));
        }
      });
    }
  }
}
