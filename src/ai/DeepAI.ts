import { AI } from './AI';
import { Game } from '../models/Game';
import { Color } from '../models/enum';
import { Move } from '../interfaces/Move';
import * as log from 'loglevel';

interface MoveCalculation {
  score: number;
  move: Move;
}

export class DeepAI implements AI {
  readonly name: string = 'DeepAI';
  private readonly logger = log.getLogger('DeepAI');
  private ii = 0;

  constructor() {
    this.logger.setLevel(log.levels.INFO);
  }

  async getBestMove(game: Game): Promise<Move> {
    this.logger.info('Starting to calculate move for color', game.toJSON().player_on_turn);
    const ngame: Game = new Game(game.toJSON());
    const best: MoveCalculation[] = this.calculateMoves(ngame, 2);
    this.logger.debug('Move Calculations', this.ii, best);
    return best[0].move;
  }

  calculateMoves(game: Game, recursionDepth: number): MoveCalculation[] {
    const maximising = game.player_on_turn === Color.white;
    const best_moves: MoveCalculation[][] = game.possible_moves
            .map((cmove: Move) => {
              this.ii = this.ii + 1;
              game.move(cmove);
              let mv: MoveCalculation[];
              const score = this.getScore(game);
              if (recursionDepth === 0) {
                mv = [ { move: cmove, score: score } ];
              } else {
                mv = [ { move: cmove, score: score }, ...this.calculateMoves(game, recursionDepth - 1) ];
              }
              game.undo();
              return mv;
            });
    const best = best_moves.sort((a, b) => {
      const last_a = a[a.length - 1];
      const last_b = b[b.length - 1];
      return maximising ? last_b.score - last_a.score : last_a.score - last_b.score;
    })[0];
    return best;
  }

  getScore(game: Game): number {
    const st = game.board.out.stones;
    const out_white = st.filter((s) => s.color === Color.white).length;
    const out_black = st.filter((s) => s.color === Color.black).length;
    return out_black - out_white  + 0.1 * (Math.random() - 0.5);
  }
}
