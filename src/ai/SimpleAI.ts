import { AI } from './AI';
import { Game } from '../models/Game';
import { Color } from '../models/enum';
import * as randomNumber from 'random-number-csprng';
import { Move } from '../interfaces/Move';
import * as log from 'loglevel';

export class SimpleAI implements AI {
    readonly name: string = 'SimpleAI';
    private readonly logger = log.getLogger('SimpleAi');

    constructor() {
        this.logger.setLevel(log.levels.INFO);
    }

    async getBestMove(game: Game): Promise<Move> {
        this.logger.info('Starting to calculate move');
        await new Promise(resolve => setTimeout(resolve, 2000));
        const number = await randomNumber(0, game.possible_moves.length - 1);
        this.logger.info('Best move', number);
        return game.possible_moves[number];
    }

    getScore(): number {
        return 0;
    }
}
