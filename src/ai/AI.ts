import { Game } from '../models/Game';
import { Color, Direction } from '../models/enum';
import { Move } from '../interfaces/Move';
import { Stone } from '../models/Stone';
import { Field } from '../models/Field';

export interface AI {
  readonly name: string;

    // get best move in game for color color
  getBestMove(game: Game): Promise<Move>;

  getScore(game: Game): number;
}

// score
// positive: white wins
// negative: black wins

export function score_out(game: Game): number {
  const st: Stone[] = game.board.out.stones;
  const out_white: number = st.filter((s) => s.color === Color.white).length;
  const out_black: number = st.filter((s) => s.color === Color.black).length;
  return out_black - out_white;
}

export function score_reserve(game: Game): number {
  return game.board.reserve_white.stones.length - game.board.reserve_black.stones.length;
}

export function score_neighbors(game: Game): number {
  let nr_neighbors_white: number = 0;
  let nr_neighbors_black: number = 0;
  for (const stone of game.board.stones) {
    const field: Field = game.board.fields.get(stone.field.id);
    if (field) {
      for (const dir of [ Direction.n, Direction.ne, Direction.nw ]) {
        const nb: Field = field.neighbor.get(dir);
        if (nb.stone) {
          if (nb.stone.color === Color.white && stone.color === Color.white) {
            nr_neighbors_white++;
          }
          if (nb.stone.color === Color.black && stone.color === Color.black) {
            nr_neighbors_black++;
          }
        }
      }
    }
  }
  return nr_neighbors_white - nr_neighbors_black;
}
