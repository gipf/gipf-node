import { AI, score_neighbors, score_out, score_reserve } from './AI';
import { AlphaBeta } from './AlphaBeta';

// TODO: add https://www.npmjs.com/package/macao

export class AiFactory {
  private static ais: string[] = [
    'AlphaBeta',
    'AlphaBeta1',
    'AlphaBeta2',
    'AlphaBeta3',
    'AlphaBeta4' ];

  static create(identifier: string): AI {
    console.log("create AI", identifier)
    switch (identifier) {
    case 'AlphaBeta':
      return new AlphaBeta({ depth: 4, score_function: score_out });
    case 'AlphaBeta1':
      return new AlphaBeta({
        depth: 3, score_function: (game) => {
          return score_out(game) + 0.1 * score_neighbors(game) + 0.2 * score_reserve(game)
        }
      });
    case 'AlphaBeta2':
      return new AlphaBeta({
        depth: 3, score_function: (game) => {
          return score_out(game) + 0.5 * score_neighbors(game) + 0.2 * score_reserve(game)
        }
      });
    case 'AlphaBeta3':
      return new AlphaBeta({
        depth: 3, score_function: (game) => {
          return score_out(game) + 0.2 * score_neighbors(game) + 0.3 * score_reserve(game)
        }
      });
    case 'AlphaBeta4':
      return new AlphaBeta({
        depth: 4, score_function: (game) => {
          return score_out(game) + 0.2 * score_neighbors(game) + 0.1 * score_reserve(game)
        }
      });

    }
  }

  static getIdentifiers(): string[] {
    return this.ais;
  }
}
