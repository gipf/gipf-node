import { AI } from './AI';
import { Game } from '../models/Game';
import { Color } from '../models/enum';
import { Move } from '../interfaces/Move';
import * as log from 'loglevel';

declare global {
  interface Array<T> {
    shuffle(): T[];
  }
}

Array.prototype.shuffle = function (): {}[] {
  const inputArray: {}[] = this;
  for (let i: number = inputArray.length - 1; i >= 0; i--) {
    const randomIndex: number = Math.floor(Math.random() * (i + 1));
    const itemAtIndex: {} = inputArray[randomIndex];

    inputArray[randomIndex] = inputArray[i];
    inputArray[i] = itemAtIndex;
  }
  return inputArray;
}

interface MoveCalculation {
  score: number;
  move: Move;
}
type ScoreFunction = (game: Game) => number;

export class AlphaBeta implements AI {
  readonly name: string = 'AlphaBeta';
  private readonly logger: log.Logger = log.getLogger('AlphaBeta');
  private ii: number = 0;
  private score_function: ScoreFunction;
  private depth: number;

  constructor(options?: {score_function: ScoreFunction, depth: number}) {
    this.logger.setLevel(log.levels.WARN);
    if (options) {
      this.score_function = options.score_function;
      this.depth = options.depth || 3;
    }
  }

  async getBestMove(game: Game): Promise<Move> {
    this.logger.info('Starting to calculate move for color', game.toJSON().player_on_turn);
    const ngame: Game = new Game(game.toJSON());
    ngame.possible_moves.shuffle();
    const best: MoveCalculation = this.calculateMoves(ngame, this.depth, -999, 999);
    this.logger.info('Move Calculations', this.ii, best);
    return best.move;
  }

  calculateMoves(game: Game, recursionDepth: number, alpha: number, beta: number): MoveCalculation {
    const maximising: boolean = game.player_on_turn === Color.white;
    let best_move: Move = undefined;

    if (recursionDepth === 0) {
      return { score: this.getScore(game), move: undefined };
    }
    if (maximising) {
      let best_score: number = -999;
      for (const cmove of game.possible_moves) {
        this.ii = this.ii + 1;
        game.move(cmove);
        const mv: MoveCalculation = this.calculateMoves(game, recursionDepth - 1, alpha, beta);
        game.undo();
        if (mv.score > best_score) {
          best_score = mv.score;
          best_move = cmove;
        }
        alpha = Math.max(mv.score, alpha);
        if (alpha >= beta) {
          this.logger.trace('beta cutoff');
          break;
        }
      }
      return { score: best_score, move: best_move };
    } else {
      let best_score: number = 999;
      for (const cmove of game.possible_moves) {
        this.ii = this.ii + 1;
        game.move(cmove);
        const mv: MoveCalculation = this.calculateMoves(game, recursionDepth - 1, alpha, beta);
        game.undo();
        if (mv.score < best_score) {
          best_score = mv.score;
          best_move = cmove;
        }
        beta = Math.min(mv.score, beta);
        if (alpha >= beta) {
          this.logger.trace('alpha cutoff');
          break;
        }
      }
      return { score: best_score, move: best_move };
    }
  }

  getScore(game: Game): number {
    return this.score_function(game);
  }
}
