import { AI } from './AI';
import { Game } from '../models/Game';
import { Color } from '../models/enum';
import { Move } from '../interfaces/Move';
import * as log from 'loglevel';
import { Stone } from '../models/Stone';

interface MoveCalculation {
    score: number;
    move: Move;
}

export class MiniMax implements AI {
    readonly name: string = 'MiniMax';
    private readonly logger;
    private ii = 0;

    constructor() {
        this.logger = log.getLogger('MiniMax');
        this.logger.setLevel(log.levels.ERROR);
    }

    async getBestMove(game: Game): Promise<Move> {
        this.logger.debug('Starting to calculate move for color', game.toJSON().player_on_turn);
        const ngame: Game = new Game(game.toJSON());
        const best: MoveCalculation = this.calculateMoves(ngame, 2);
        this.logger.debug('Move Calculations', this.ii, best);
        return best.move;
    }

    calculateMoves(game: Game, recursionDepth: number): MoveCalculation {
        const maximising: boolean = game.player_on_turn === Color.white;
        let best_score: number = maximising ? -999 : 999;
        let best_move: Move = undefined;

        game.possible_moves.forEach((cmove: Move) => {
            this.ii = this.ii + 1;
            game.move(cmove);
            let score: number;
            if (recursionDepth === 0) {
                score = this.getScore(game);
            } else {
                score = this.calculateMoves(game, recursionDepth - 1).score;
            }
            if (maximising && score > best_score) {
                best_score = score;
                best_move = cmove;
            }
            if (!maximising && score < best_score) {
                best_score = score;
                best_move = cmove;
            }
            game.undo();
        });
        return { score: best_score, move: best_move };
    }

    getScore(game: Game): number {
        const st: Stone[] = game.board.out.stones;
        const out_white: number = st.filter((s) => s.color === Color.white).length;
        const out_black: number = st.filter((s) => s.color === Color.black).length;
        return out_black - out_white; //  + 0.1 * ( Math.random() -0.5);
    }
}
