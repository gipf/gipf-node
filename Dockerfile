# Docker Parent Image with Node and Typescript
FROM node:alpine as base
# Create Directory for the Container
WORKDIR /app

# image for runtime dependencies
FROM base as dependencies
COPY package.json .
COPY package-lock.json .
RUN npm install --prod

## Image for building
FROM dependencies as build
COPY src src
COPY tsconfig.json .
RUN npm install
RUN npm run compile
RUN npm run apidoc

# production image
FROM node:alpine
WORKDIR /app
COPY --from=dependencies /app/node_modules node_modules
COPY --from=build /app/build build
COPY --from=build /app/apidoc apidoc
VOLUME [ "/app/.node-persist" ]
EXPOSE 3000
CMD [ "build" ]
