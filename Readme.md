gipf-node
=========

Usage
-----

```
npm run
```
opens a server on port 3000

API doc can be accessed via http://localhost:3000/doc


Gipf
----


```
• - dot to push from
○ - white stone
● - black stone
◎ - white gipf stone
◉ - black gipf stone 

white: 5/7/3            black: 5/7/3

           i5  i4  i3  i2  i1
            •   •   •   •   •
             ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱
       h6 •--- --- --- --- ---• h1
           ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ 
     g7 •--- --- --- --- --- ---• g1
         ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱
   f8 •---●---●---◉--- --- --- ---• f1
       ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱
 e9 •---○---○---●---○--- --- --- ---• e1
       ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲
   d8 •--- --- --- --- --- --- ---• d1
         ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲
     c7 •--- --- --- --- --- ---• c1
           ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲
       b6 •--- --- --- --- ---• b1
             ╱ ╲ ╱ ╲ ╱ ╲ ╱ ╲
            •   •   •   •   •
           a5  a4  a3  a2  a1
```
