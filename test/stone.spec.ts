import { Stone } from '../src/models/Stone';
import { expect } from 'chai';
import * as assert from 'assert';
import { Color } from '../src/models/enum';

describe('StoneTest', () => {

    it('should create', () => {
        const stone: Stone = new Stone(3, 'white' as Color);
        assert.equal(stone.color, 'white');
    });

    it('should change', () => {
        const stone: Stone = new Stone(6, 'black' as Color);
        expect(stone.id).to.equal(6);
        expect(stone.color).to.equal('black');
        expect(stone.gipf).to.equal(false);

        const stone2: Stone = new Stone(2, 'white' as Color);
        expect(stone2.color).to.equal('white');
        expect(stone2.toJson()).to.deep.equal({
            color: 'white',
            field: null,
            gipf: false,
            id: 2
        });
    });
});
