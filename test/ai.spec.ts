import { Color, Direction } from '../src/models/enum';
import { Game } from '../src/models/Game';
import { PushMove } from '../src/interfaces/Move';
import { AI } from '../src/ai/AI';
import { expect } from 'chai';
import { AiFactory } from '../src/ai/AiFactory';
import { GameType } from '../src/models/GameType';

describe('AITest', () => {

  const AIUT = [ 'AlphaBeta', 'AlphaBeta1', 'AlphaBeta2', 'AlphaBeta3' ];

  for (const ait of AIUT) {
    describe(ait, () => {

      let ai: AI;
      before(() => {
        ai = AiFactory.create(ait);
        console.log(ai);
      });

      it('should find good move', async() => {
        const game: Game = new Game({ player_white: 'playerW', player_black: 'playerB', type: GameType.Simple});
        const moveWhite: PushMove = { type: 'PushMove', color: Color.white, field_id: 'a3', direction: Direction.se };
        const moveBlack: PushMove = { type: 'PushMove', color: Color.black, field_id: 'a2', direction: Direction.se };

        game.move(moveWhite);
        game.move(moveBlack);
        game.move(moveWhite);
        game.move(moveBlack);
        game.move(moveWhite);
        game.move(moveBlack);

        const best = await ai.getBestMove(game);
      }).timeout(120000);

      it('should find recover move for white', async () => {
        const game: Game = new Game({ player_white: 'playerW', player_black: 'playerB', type: GameType.Simple });
        game.set_root_position({ white: [ 'e2', 'e3', 'e4' ], black: [ 'e5', 'e6' ] }, Color.white);

        const bestWhite = await ai.getBestMove(game);
        expect(bestWhite).to.deep.equal(
            { type: 'PushMove', color: Color.white, field_id: 'e1', direction: Direction.n });
      }).timeout(10000);

      it('should find recover move for black', async () => {
        const game: Game = new Game({ player_white: 'playerW', player_black: 'playerB', type: GameType.Simple });
        game.set_root_position({ black: [ 'e2', 'e3', 'e4' ], white: [ 'e5', 'e6' ] }, Color.black);

        const bestWhite = await ai.getBestMove(game);
        expect(bestWhite).to.deep.equal(
            { type: 'PushMove', color: Color.black, field_id: 'e1', direction: Direction.n });
      }).timeout(10000);

      it('should find best move for specific position for black', async () => {
        const game: Game = new Game({ player_white: 'playerW', player_black: 'playerB', type: GameType.Simple });
        game.set_root_position({ white: [ 'e2', 'e3', 'e4' ], black: [ 'e5', 'e6' ] }, Color.black);

        const bestBlack = await ai.getBestMove(game);
        expect(bestBlack)
                    .to.include({ type: 'PushMove', color: 'black' })
                    .and.to.satisfy((move: PushMove) => (
                        (move.field_id === 'd1' && move.direction === Direction.ne) ||
                    (move.field_id === 'f1' && move.direction === Direction.nw)
                    ));
      }).timeout(30000);

    });
  }
});
