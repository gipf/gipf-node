import { Board } from '../src/models/Board';
import * as storage from 'node-persist';
import { expect } from 'chai';

describe('StorageTest', () => {

    it('should create', () => {
        const board: Board = new Board();
        expect(board.fields).to.have.lengthOf(61);
        expect(board.stones).to.have.lengthOf(30);
        expect(board.reserve_black.stones).to.have.lengthOf(15);
        expect(board.reserve_white.stones).to.have.lengthOf(15);
    });

    it('should load and restore', async () => {
        const board_original: Board = new Board();

        await storage.init();
        await storage.set('1', board_original.toJSON());
        const board: Board = new Board(await storage.get('1'));

        expect(board.fields).to.have.lengthOf(61);
        expect(board.stones).to.have.lengthOf(30);
        expect(board.reserve_black.stones).to.have.lengthOf(15);
        expect(board.reserve_white.stones).to.have.lengthOf(15);

        expect(board.toJSON()).to.deep.equal(board_original.toJSON());
    });

});
