import * as assert from 'assert';
import {Color, Direction} from '../src/models/enum';
import {Game} from '../src/models/Game';
import {expect} from 'chai';
import {spy, when} from "ts-mockito";
import {PushMove, RecoverMove} from "../src/interfaces/Move";
import {AI} from "../src/ai/AI";
import {DeepAI} from "../src/ai/DeepAI";

describe('GameTest', () => {

    it('should succesfully create', () => {
        const game: Game = new Game({ player_white: 'playerW', player_black: 'playerB' });
        expect(game.turnNumber).to.equal(1);
        expect(game.datetime_last).to.equal(undefined);
        expect(game.history).to.have.length(0);
        expect(game.winner).to.equal(null);
        expect(game.player_on_turn).to.equal(Color.white);
        expect(game.possible_moves).to.have.length(42);
    });

    it('should move with board as integration test', () => {
        const game1: Game = new Game({ player_white: '111', player_black: 'edfs' });
        game1.move({ type: 'PushMove', color: Color.white, field_id: 'a2', direction: Direction.se });
        assert.equal(game1.board.fields.get('b2').stone.color, Color.white);
        expect(game1.winner).to.equal(null);

        const game2: Game = new Game(game1.toJSON());
        assert.equal(game2.board.fields.size, 61);
        assert.equal(game2.board.stones.length, 30);
        assert.equal(game2.board.fields.get('b2').stone.color, Color.white);
    });

    it('should do some moves', () => {
        const game: Game = new Game({ player_white: '111', player_black: 'edfs' });
        const json = game.toJSON();
        expect(json.player_on_turn).to.equal(Color.white);
        game.move({ type: 'PushMove', color: Color.white, field_id: 'a2', direction: Direction.se });
        expect(game.toJSON().player_on_turn).to.equal(Color.black);
        expect(game.toJSON().possible_moves[0].color).to.equal(Color.black);
    });

    it('should do switch user appropriately', () => {
        const game: Game = new Game({ player_white: '111', player_black: 'edfs' });
        const spiedBoard = spy(game.board);

        const moveWhite: PushMove = {type: 'PushMove', color: Color.white, field_id: 'a2', direction: Direction.ne};
        const moveBlack: PushMove = {type: 'PushMove', color: Color.black, field_id: 'a2', direction: Direction.ne};
        const moveRecoverWhite: RecoverMove = {type: 'RecoverMove', recovered_field_ids: [], captured_field_ids: [], color: Color.white};
        const moveRecoverBlack: RecoverMove = {type: 'RecoverMove', recovered_field_ids: [], captured_field_ids: [], color: Color.black};

        expect(game.possible_moves[0]).to.have.property('color', 'white');
        expect(game.player_on_turn).to.equal(Color.white);
        expect(game.possible_moves).to.have.length(42);

        game.move(moveWhite);
        expect(game.player_on_turn).to.equal(Color.black);
        expect(game.possible_moves).to.have.length(42);
        expect(() => game.move(moveWhite)).to.throw('Move not possible');
        expect(() => game.move(moveRecoverWhite)).to.throw('Move not possible');
        expect(() => game.move(moveRecoverBlack)).to.throw('Move not possible');

        game.move(moveBlack);
        game.move(moveWhite);

        when(spiedBoard.getPossibleRecoverMoves(Color.white)).thenReturn([moveRecoverWhite]);
        when(spiedBoard.getPossibleRecoverMoves(Color.black)).thenReturn([moveRecoverBlack]);
        game.move(moveBlack);
        expect(game.player_on_turn).to.equal(Color.black);
        expect(game.possible_moves).to.have.length(1);
        expect(game.possible_moves).to.deep.equal([moveRecoverBlack]);

        game.move(moveRecoverBlack);
        expect(game.player_on_turn).to.equal(Color.black);
        expect(game.possible_moves).to.deep.equal([moveRecoverBlack]);

        when(spiedBoard.getPossibleRecoverMoves(Color.white)).thenReturn([moveRecoverWhite]);
        when(spiedBoard.getPossibleRecoverMoves(Color.black)).thenReturn([]);
        game.move(moveRecoverBlack);
        expect(game.player_on_turn).to.equal(Color.white);
        expect(game.possible_moves).to.deep.equal([moveRecoverWhite]);

        when(spiedBoard.getPossibleRecoverMoves(Color.white)).thenReturn([]);
        when(spiedBoard.getPossibleRecoverMoves(Color.black)).thenReturn([]);
        game.move(moveRecoverWhite);
        expect(game.player_on_turn).to.equal(Color.white);
        expect(game.possible_moves).to.have.length(42);

        game.move(moveWhite);
    });

    it('should copy game', () => {
        const game: Game = new Game({player_white: 'playerW', player_black: 'playerB'});
        const ai: AI = new DeepAI();

        const moveWhite: PushMove = {type: 'PushMove', color: Color.white, field_id: 'a2', direction: Direction.ne};
        const moveBlack: PushMove = {type: 'PushMove', color: Color.black, field_id: 'a3', direction: Direction.ne};

        game.move(moveWhite);
        game.move(moveBlack);
        game.move(moveWhite);
        game.move(moveBlack);

        const ngame = new Game(game.toJSON());

        const moves = game.getMovePossibilities();
        const nmoves = ngame.getMovePossibilities();

        expect(moves).to.deep.equal(nmoves);
        expect(game.toJSON()).to.deep.equal(ngame.toJSON());
        expect(game.history).to.have.length(4);
        expect(ngame.history).to.have.length(4);

        ngame.move(moveWhite);
        expect(game.toJSON()).to.not.deep.equal(ngame.toJSON());

        expect(game.history).to.have.length(4);
        expect(ngame.history).to.have.length(5);
    });

    it('should undo moves 2', () => {
        const game: Game = new Game({player_white: 'playerW', player_black: 'playerB'});

        const moveWhite: PushMove = {type: 'PushMove', color: Color.white, field_id: 'a2', direction: Direction.ne};
        const moveBlack: PushMove = {type: 'PushMove', color: Color.black, field_id: 'a3', direction: Direction.ne};

        game.move(moveWhite);
        game.move(moveBlack);
        game.move(moveWhite);
        game.move(moveBlack);

        const json = {... game.toJSON()};
        const stones = [ ... game.toJSON().stones];

        game.move(moveWhite);

        expect(game.toJSON()).to.not.deep.equal(json);
        expect(game.toJSON().stones).to.not.deep.equal(stones);

        game.undo();

        expect(game.toJSON()).to.deep.equal(json);
        expect(game.toJSON().stones).to.deep.equal(stones);

        game.move(moveWhite);
        game.move(moveBlack);

        game.undo();
        game.undo();

        expect(game.toJSON()).to.deep.equal(json);
        expect(game.toJSON().stones).to.deep.equal(stones);
    });

    it('should undo moves', () => {
        const game: Game = new Game({player_white: 'playerW', player_black: 'playerB'});
        game.move({ type: 'PushMove',
            color: Color.white,
            field_id: 'a2',
            direction: Direction.ne });
        game.move({ type: 'PushMove',
            color: Color.black,
            field_id: 'a3',
            direction: Direction.ne });
        game.move({ type: 'PushMove',
            color: Color.white,
            field_id: 'a2',
            direction: Direction.ne });
        game.move({ type: 'PushMove',
            color: Color.black,
            field_id: 'a3',
            direction: Direction.ne });
        game.move({ type: 'PushMove',
            color: Color.white,
            field_id: 'a2',
            direction: Direction.ne });
        game.move({ type: 'PushMove',
            color: Color.black,
            field_id: 'a3',
            direction: Direction.ne });
        game.move({ type: 'PushMove',
            field_id: 'a1',
            direction: Direction.ne,
            color: Color.white });
        game.move({ type: 'PushMove',
            field_id: 'a1',
            direction: Direction.ne,
            color: Color.black });
        const json = {... game.toJSON()};

        game.move({ type: 'PushMove',
            field_id: 'a1',
            direction: Direction.ne,
            color: Color.white });

        game.undo();
        expect(game.toJSON()).to.deep.equal(json);
    });

    it('should undo moves 3', () => {
        const game: Game = new Game({player_white: 'playerW', player_black: 'playerB'});
        game.move({
            type: 'PushMove',
            color: Color.white,
            field_id: 'a2',
            direction: Direction.ne
        });
        expect(game.board.fields.get('b3').is_free()).to.be.false;
        expect(game.board.fields.get('b2').is_free()).to.be.true;

        const json = game.toJSON();
        game.move({
            type: 'PushMove',
            color: Color.black,
            field_id: 'b1',
            direction: Direction.n
        });
        expect(game.board.fields.get('b3').is_free()).to.be.false;
        expect(game.board.fields.get('b2').is_free()).to.be.false;

        // should undo move without pulling stone from b3 to b2
        game.undo();
        expect(game.board.fields.get('b3').is_free()).to.be.false;
        expect(game.board.fields.get('b2').is_free()).to.be.true;
        expect(game.toJSON()).to.deep.equal(json);
    });
});
