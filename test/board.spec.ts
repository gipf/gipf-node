import * as assert from 'assert';
import { expect } from 'chai';
import { Board } from '../src/models/Board';
import { Color, Direction } from '../src/models/enum';
import { RecoverMoveImpl } from '../src/models/RecoverMove';
import { RecoverMove } from '../src/interfaces/Move';

describe('BoardTest', () => {

    it('should successfully create board', () => {
        const board: Board = new Board();
        assert.equal(board.fields.size, 61);
        assert.equal(board.stones.length, 30);
        expect([...board.fields.values()].filter((f) => f.border)).to.have.length(24);
        expect([...board.fields.values()].filter((f) => !f.border)).to.have.length(61-24);
    });

    it('should make some moves', () => {
        const board: Board = new Board();
        board.performPushMove({ type: 'PushMove', color: Color.black, field_id: 'a2', direction: Direction.se });
        board.performPushMove({ type: 'PushMove', color: Color.white, field_id: 'a2', direction: Direction.se });
        board.performPushMove({ type: 'PushMove', color: Color.black, field_id: 'a2', direction: Direction.se });
        assert.equal(board.fields.get('a2').stone, null);
        assert.equal(board.fields.get('b2').stone.color, Color.black);
        assert.equal(board.fields.get('c2').stone.color, Color.white);
        assert.equal(board.fields.get('d2').stone.color, Color.black);
        assert.equal(board.fields.get('e2').stone, null);
        board.performPushMove({ type: 'PushMove', color: Color.black, field_id: 'a2', direction: Direction.se });
        assert.equal(board.fields.get('b2').nrOfStonesInRowWithColor(Color.black, Direction.se), 2);
        assert.equal(board.fields.get('d2').nrOfStonesInRowWithColor(Color.black, Direction.se), 0);
        assert.equal(board.fields.get('b2').nrOfStonesInRowWithColor(Color.white, Direction.se), 0);

        expect(() => board.performPushMove({ type: 'PushMove', color: Color.black, field_id: 'a2', direction: Direction.se }))
            .to.throw("No field free in line");
        assert.equal(board.fields.get('b2').nrOfStonesInRowWithColor(Color.black, Direction.se), 2);

        board.performPushMove({ type: 'PushMove', color: Color.black, field_id: 'f1', direction: Direction.n });
        board.performPushMove({ type: 'PushMove', color: Color.black, field_id: 'f1', direction: Direction.n });
        board.performPushMove({ type: 'PushMove', color: Color.black, field_id: 'f1', direction: Direction.n });
        board.performPushMove({ type: 'PushMove', color: Color.black, field_id: 'f1', direction: Direction.n });
        board.performPushMove({ type: 'PushMove', color: Color.black, field_id: 'f1', direction: Direction.n });

        const takings: RecoverMove[] = board.getPossibleRecoverMoves(Color.black);
        console.log(takings)
        expect(takings.length).to.equal(1);
        expect(takings[0].color).to.equal(Color.black);
    });

    it('should provide correct possible moves for initial state', () => {
        const board: Board = new Board();
        expect(board.getPossiblePushMoves()).to.have.lengthOf(42);
    });

    it('should provide correct stones in row', () => {
        const board = new Board();
        board.reserve_white.stones[4].set(board.fields.get('c2'));
        board.reserve_white.stones[4].set(board.fields.get('c3'));
        board.reserve_black.stones[4].set(board.fields.get('c4'));
        board.reserve_white.stones[4].set(board.fields.get('c5'));
        board.reserve_black.stones[4].set(board.fields.get('c6'));
        board.reserve_white.stones[4].set(board.fields.get('d3'));

        expect(board.fields.get('c2').getStonesInRow(Direction.n)).to.have.length(5);
        expect(board.fields.get('c2').getStonesInRow(Direction.ne)).to.have.length(2);
        expect(board.fields.get('c2').getStonesInRow(Direction.se)).to.have.length(1);
        expect(board.fields.get('e2').getStonesInRow(Direction.n)).to.have.length(0);
    });

    it('should provide correct open takings 2', () => {
        const board: Board = new Board();
        board.reserve_white.stones[4].set(board.fields.get('d3'));
        board.reserve_white.stones[4].set(board.fields.get('d4'));
        board.reserve_white.stones[4].set(board.fields.get('c2'));
        board.reserve_white.stones[4].set(board.fields.get('c3'));
        board.reserve_white.stones[4].set(board.fields.get('c4'));
        board.reserve_white.stones[4].set(board.fields.get('c5'));
        board.reserve_black.stones[4].set(board.fields.get('c6'));

        const t = board.getPossibleRecoverMoves(Color.white);
        expect(t).to.have.length(1);
        const t0 = t[0];
        expect(t0.color).to.equal(Color.white);
        expect(t0.recovered_field_ids.length).to.equal(4);
        expect(t0.captured_field_ids.length).to.equal(1);

        board.performRecoverMove(t0);

        expect(board.fields.get('c3').stone).to.equal(null);

    });

    it('should provide correct possible recover moves', () => {
        const board: Board = new Board();
        board.reserve_white.stones[4].set(board.fields.get('d3'));
        board.reserve_white.stones[4].set(board.fields.get('d4'));
        board.reserve_white.stones[4].set(board.fields.get('c2'));
        board.reserve_white.stones[4].set(board.fields.get('c3'));
        board.reserve_white.stones[4].set(board.fields.get('c4'));
        board.reserve_white.stones[4].set(board.fields.get('c5'));
        board.reserve_black.stones[4].set(board.fields.get('c6'));

        const o = RecoverMoveImpl.fromField(board.fields.get('c2'), Color.white, Direction.n);

        const abc: RecoverMove = {
            type: 'RecoverMove',
            captured_field_ids: [
                'c6'
            ],
            color: Color.white,
            recovered_field_ids: [
                'c2',
                'c3',
                'c4',
                'c5'
            ]
        };

        expect(o).to.deep.equal(abc);

        const o2 = new RecoverMoveImpl(abc.color, abc.recovered_field_ids, abc.captured_field_ids);
        expect(o.equals(o2)).to.equal(true);

        const o3 = new RecoverMoveImpl(Color.black, abc.recovered_field_ids, abc.captured_field_ids);
        expect(o.equals(o3)).to.equal(false);
    });
});
