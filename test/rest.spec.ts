import { Server } from '../src/server/server';

import { expect } from 'chai';
// tslint:disable: no-unused-expression

import { describe, it } from 'mocha';
import { Manager } from '../src/manager/Manager';
import axios, { AxiosResponse } from 'axios';

describe('RestTest', () => {

  let gipfApp: Server;
  const port: number = 4200;

  before(() => {
    gipfApp = new Server(new Manager());
    gipfApp.startHttpServer(port);
  });

  after(async () => {
    await gipfApp.stop();
  });

  it('it should GET games', async () => {
    const response: AxiosResponse = await axios.get('http://127.0.0.1:4200/api/games');
    expect(response.status).to.equal(200);
    expect(response.data).to.be.an('array').that.is.empty;
  });
});
